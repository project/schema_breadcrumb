Schema breadcrumb module
------------------------

Installation
------------
 * install module
 * add the theme_breadcrumb function to your theme template.php

theme_breadcrumb
----------------
'''
function theme_breadcrumb($variables) {

  $breadcrumb = $variables['breadcrumb'];

  return theme('schema_breadcrumb', array(
      'attributes' => array(
        'class' => array('breadcrumb'),
      ),
      'items' => $breadcrumb,
      'type' => 'ol',
    )
  );

}
'''
